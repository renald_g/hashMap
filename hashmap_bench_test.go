package hashMap

import (
	"testing"
)

func setupSet(b *testing.B, blockSize int, fn func(blockSize int, key Key) int) {
	hashMap := NewHashMap(blockSize, fn)

	for i := 0; i < b.N; i++ {

		b.StopTimer()
		key := randString(strLength)
		b.StartTimer()

		hashMap.Set(key, i)
	}
}

func BenchmarkSet16(b *testing.B) {
	setupSet(b, 16, hashString)
}

func BenchmarkSet64(b *testing.B) {
	setupSet(b, 64, hashString)
}

func BenchmarkSet128(b *testing.B) {
	setupSet(b, 128, hashString)
}

func BenchmarkSet1024(b *testing.B) {
	setupSet(b, 1024, hashString)
}

func BenchmarkSet1024Universal(b *testing.B) {
	setupSet(b, 1024, universalHashString)
}

func BenchmarkSet1024SDBM(b *testing.B) {
	setupSet(b, 1024, sdbmHash)
}

func BenchmarkNativeMapSet(b *testing.B) {
	mapNative := make(map[Key]int)
	for i := 0; i < b.N; i++ {

		b.StopTimer()
		key := randString(strLength)
		b.StartTimer()

		mapNative[key] = i
	}
}

func setupGet(b *testing.B, blockSize int, fn func(blockSize int, key Key) int) {
	hashMap := NewHashMap(blockSize, fn)

	b.StopTimer()
	var keys []Key

	for i := 0; i < b.N; i++ {
		key := randString(strLength)
		hashMap.Set(key, string(i))
		keys = append(keys, key)
	}
	b.StartTimer()

	for _, val := range keys {
		_, err := hashMap.Get(val)
		if err != nil {
			b.Errorf("Key %s not found for %d size", val, blockSize)
		}
	}
}

func BenchmarkGet16(b *testing.B) {
	setupGet(b, 16, hashString)
}

func BenchmarkGet64(b *testing.B) {
	setupGet(b, 64, hashString)
}

func BenchmarkGet128(b *testing.B) {
	setupGet(b, 128, hashString)
}

func BenchmarkGet1024(b *testing.B) {
	setupGet(b, 1024, hashString)
}

func BenchmarkGet1024Universal(b *testing.B) {
	setupGet(b, 1024, universalHashString)
}

func BenchmarkGet1024SDBM(b *testing.B) {
	setupGet(b, 1024, sdbmHash)
}

func BenchmarkNativeMapGet(b *testing.B) {
	mapNative := make(map[Key]int)

	b.StopTimer()
	var keys []Key
	for i := 0; i < b.N; i++ {
		key := randString(strLength)
		mapNative[key] = i
		keys = append(keys, key)
	}
	b.StartTimer()

	for _, val := range keys {
		_ = mapNative[val];
	}
}

func setupUnset(b *testing.B, blockSize int) {
	hashMap := NewHashMap(blockSize)

	b.StopTimer()
	var keys []Key

	for i := 0; i < b.N; i++ {
		key := randString(strLength)
		hashMap.Set(key, string(i))
		keys = append(keys, key)
	}
	b.StartTimer()

	for _, val := range keys {
		hashMap.Unset(val)
	}
}

func BenchmarkUnset16(b *testing.B) {
	setupUnset(b, 16)
}

func BenchmarkUnset64(b *testing.B) {
	setupUnset(b, 64)
}

func BenchmarkUnset128(b *testing.B) {
	setupUnset(b, 128)
}

func BenchmarkUnset1024(b *testing.B) {
	setupUnset(b, 1024)
}

func BenchmarkNativeMapUnset(b *testing.B) {
	mapNative := make(map[Key]int)

	b.StopTimer()
	var keys []Key
	for i := 0; i < b.N; i++ {
		key := randString(strLength)
		mapNative[key] = i
		keys = append(keys, key)
	}
	b.StartTimer()

	for _, val := range keys {
		delete(mapNative, val)
	}
}

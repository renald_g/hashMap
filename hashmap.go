package hashMap

import (
	"errors"
)

type Key string

const (
	nilIndex = -1
	loadFactor = 0.66
	minFactor = 0.33
)

type HashNode struct {
	key			Key
	value		interface{}
	deleted		bool
}

type HashMapper interface {
	Set(key Key, value interface{}) error
	Get(key Key) (value interface{}, err error)
	Unset(key Key) error
	Count() int
}

type HashMap struct {
	hashFunc			func(blockSize int, key Key) int
	containerSize		int
	container			[]*HashNode
	size				int
	deletedSize			int
}

func NewHashMap(blockSize int, fn ...func(blockSize int, key Key) int) HashMapper {
	hashMap := new(HashMap)
	hashMap.containerSize = blockSize
	hashMap.container = make([]*HashNode, hashMap.containerSize)
	hashMap.size = 0
	hashMap.deletedSize = 0
	if len(fn) > 0 {
		hashMap.hashFunc = fn[0]
	} else {
		hashMap.hashFunc = hashString
	}

	return hashMap
}

func (hm *HashMap) findEntry(key Key) (index int, element *HashNode)  {
	hashVal := hm.hashFunc(hm.containerSize, key)
	rootIndex := hashVal
	for offset := 0; offset <= hm.containerSize; offset++ {
		index := (rootIndex + offset) % hm.containerSize
		element = hm.container[index]
		if element == nil || element.deleted == true || element.key == key  {
			return index, element
		}
	}

	return nilIndex, nil
}

func (hm *HashMap) resize() {
	oldContainer := hm.container
	oldSize := hm.size
	hm.containerSize = int(float64(oldSize) / minFactor)
	hm.container = make([]*HashNode, hm.containerSize)
	hm.size = 0
	hm.deletedSize = 0
	for _, element := range oldContainer {
		if element != nil && element.deleted != true {
			hm.Set(element.key, element.value)
		}
	}
}

func (hm *HashMap) Set(key Key, value interface{}) error {
	index, element := hm.findEntry(key)
	if index == nilIndex {
		errors.New("Insertion error!")
	}

	hm.container[index] = &HashNode{key:key, value:value}
	if element == nil {
		hm.size++
	}

	if float64(hm.deletedSize + hm.size) / float64(hm.containerSize) > loadFactor {
		hm.resize()
	}

	return nil
}

func (hm *HashMap) Get(key Key) (value interface{}, err error) {
	_, element := hm.findEntry(key)
	if element != nil {
		return element.value, nil
	}

	return nil, errors.New("Key not found!")
}

func (hm *HashMap) Unset(key Key) error {
	index, element := hm.findEntry(key)
	if element != nil {
		hm.container[index].deleted = true
		hm.size--
		hm.deletedSize++
		return nil
	}

	return errors.New("Key not found!")
}

func (hm *HashMap) Count() int {
	return hm.size
}


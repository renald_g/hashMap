### Test

```bash
$ go test hashmap_bench_test.go hashfunc.go hashmap.go -bench=. -benchmem -cpu 1

BenchmarkSet16                   1000000              2214 ns/op             168 B/op          3 allocs/op
BenchmarkSet64                   1000000              1715 ns/op             165 B/op          3 allocs/op
BenchmarkSet128                  1000000              1726 ns/op             164 B/op          3 allocs/op
BenchmarkSet1024                 1000000              1527 ns/op             164 B/op          3 allocs/op
BenchmarkSet1024Universal        1000000              2024 ns/op             164 B/op          3 allocs/op
BenchmarkSet1024SDBM             1000000              1425 ns/op             164 B/op          3 allocs/op
BenchmarkNativeMapSet            2000000               830 ns/op             123 B/op          0 allocs/op
BenchmarkGet16                   5000000               468 ns/op               0 B/op          0 allocs/op
BenchmarkGet64                   5000000               514 ns/op               0 B/op          0 allocs/op
BenchmarkGet128                  5000000               482 ns/op               0 B/op          0 allocs/op
BenchmarkGet1024                 5000000               491 ns/op               0 B/op          0 allocs/op
BenchmarkGet1024Universal        3000000               422 ns/op               0 B/op          0 allocs/op
BenchmarkGet1024SDBM             5000000               387 ns/op               0 B/op          0 allocs/op
BenchmarkNativeMapGet           20000000               130 ns/op               0 B/op          0 allocs/op
BenchmarkUnset16                 5000000               373 ns/op               0 B/op          0 allocs/op
BenchmarkUnset64                 5000000               365 ns/op               0 B/op          0 allocs/op
BenchmarkUnset128                5000000               348 ns/op               0 B/op          0 allocs/op
BenchmarkUnset1024               5000000               358 ns/op               0 B/op          0 allocs/op
BenchmarkNativeMapUnset         10000000               172 ns/op               0 B/op          0 allocs/op
```
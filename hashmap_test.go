package hashMap

import (
	"testing"
	"strconv"
)

const operationCount = 1000000


func TestSet(t *testing.T) {
	for _, size := range [4]int{16, 64, 128, 1024} {
		hashMap := NewHashMap(size)

		for i := 0; i < operationCount; i++ {
			key := randString(strLength)
			err := hashMap.Set(key, i)
			if err != nil {
				t.Errorf("Set error for %d size on key %s", size, key)
			}
		}
	}
}


func TestGet(t *testing.T) {
	for _, size := range [4]int{16, 64, 128, 1024} {
		hashMap := NewHashMap(size)
		var keys [operationCount]Key

		for i := 0; i < operationCount; i++ {
			key := randString(strLength)
			err := hashMap.Set(key, string(i))
			keys[i] = key
			if err != nil {
				t.Errorf("Set error for %d size on key %s", size, key)
			}
		}

		for _, val := range keys {
			_, err := hashMap.Get(val)
			if err != nil {
				t.Errorf("Key %s not found for %d size", val, size)
			}
		}
	}
}


func TestUnset(t *testing.T) {
	for _, size := range [4]int{16, 64, 128, 1024} {
		hashMap := NewHashMap(size)
		var keys [operationCount]Key

		for i := 0; i < operationCount; i++ {
			key := randString(strLength) + Key(strconv.Itoa(i))
			err := hashMap.Set(key, string(i))
			keys[i] = key
			if err != nil {
				t.Errorf("Set error for %d size on key %s", size, key)
			}
		}

		for _, val := range keys {
			err := hashMap.Unset(val)
			if err != nil {
				t.Errorf("Key %s not found for %d size", val, size)
			}
		}
	}
}

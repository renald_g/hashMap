package hashMap


func hashString(blockSize int, key Key) int {
	a, h := 127, 0
	for _, char := range key {
		h = (a * h + int(char)) % blockSize
	}
	return h;
}


func universalHashString(blockSize int, key Key) int {
	h, a, b := 0, 31415, 27183
	for _, char := range key {
		h = (a * h + int(char)) % blockSize
		a = a*b % (blockSize-1)
	}

	if h < 0 {
		return (h + blockSize)
	} else {
		return h
	}
}

func sdbmHash(blockSize int, key Key) int {
	var h uint = 0
	for _, char := range key {
		h = uint(char) + (h << 6) + (h << 16) - h;
	}

	return int(h % uint(blockSize))
}
